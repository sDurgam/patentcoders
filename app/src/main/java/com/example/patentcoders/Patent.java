package com.example.patentcoders;

public class Patent {
        public String id;
        public String Jurisdiction ;
        public String Kind ;
        public String PublicationNumber ;
        public String LensID ;
        public String PublicationDate ;
        public String PublicationYear ;
        public String ApplicationNumber ;
        public String ApplicationDate ;
        public String PriorityNumbers ;
        public String EarliestPriorityDate ;
        public String Title ;
        public String Applicants ;
        public String Inventors ;
        public String URL ;
        public String Type ;
        public String Abstract ;
        public String MainClaim ;
        public String HasFullText ;
        public String CitedbyPatentCount ;
        public String SimpleFamilySize ;
        public String ExtendedFamilySize ;
        public String SequenceCount ;
        public String CPCClassifications ;
        public String IPCRClassifications ;
        public String USClassifications ;
        public String NPLCitationCount ;
        public String NPLResolvedCitationCount ;
        public String NPLResolvedLensIDs ;
        public String NPLResolvedExternalIds ;
        public String NPLCitations ;

    public Patent(String csvStr) {
        String[] fields = csvStr.split(",");
        int i = 0;
        this.id = fields[i++];
        this.Jurisdiction  = fields[i++];
        this.Kind = fields[i++];
        this.PublicationNumber = fields[i++];
        this.LensID = fields[i++];
        this.PublicationDate = fields[i++];
        this.PublicationYear = fields[i++];
        this.ApplicationNumber = fields[i++];
        this.ApplicationDate = fields[i++];
        this.PriorityNumbers = fields[i++];
        this.EarliestPriorityDate = fields[i++];
        this.Title = fields[i++];
        this.Applicants = fields[i++];
        this.Inventors = fields[i++];
        this.URL = fields[i++];
        this.Type = fields[i++];
        this.Abstract = fields[i++];
        this.MainClaim = fields[i++];
        this.HasFullText = fields[i++];
        this.CitedbyPatentCount = fields[i++];
        this.SimpleFamilySize = fields[i++];
        this.ExtendedFamilySize = fields[i++];
        this.SequenceCount = fields[i++];
        this.CPCClassifications = fields[i++];
        this.IPCRClassifications = fields[i++];
        this.USClassifications = fields[i++];
        this.NPLCitationCount = fields[i++];
        this.NPLResolvedCitationCount = fields[i++];
        this.NPLResolvedLensIDs = fields[i++];
        this.NPLResolvedExternalIds = fields[i++];
        this.NPLCitations = fields[i++];
    }

    public String getJurisdiction() {
        return Jurisdiction;
    }

    public void setJurisdiction(String jurisdiction) {
        Jurisdiction = jurisdiction;
    }

    public String getKind() {
        return Kind;
    }

    public void setKind(String kind) {
        Kind = kind;
    }

    public String getPublicationNumber() {
        return PublicationNumber;
    }

    public void setPublicationNumber(String publicationNumber) {
        PublicationNumber = publicationNumber;
    }

    public String getLensID() {
        return LensID;
    }

    public void setLensID(String lensID) {
        LensID = lensID;
    }

    public String getPublicationDate() {
        return PublicationDate;
    }

    public void setPublicationDate(String publicationDate) {
        PublicationDate = publicationDate;
    }

    public String getPublicationYear() {
        return PublicationYear;
    }

    public void setPublicationYear(String publicationYear) {
        PublicationYear = publicationYear;
    }

    public String getApplicationNumber() {
        return ApplicationNumber;
    }

    public void setApplicationNumber(String applicationNumber) {
        ApplicationNumber = applicationNumber;
    }

    public String getApplicationDate() {
        return ApplicationDate;
    }

    public void setApplicationDate(String applicationDate) {
        ApplicationDate = applicationDate;
    }

    public String getPriorityNumbers() {
        return PriorityNumbers;
    }

    public void setPriorityNumbers(String priorityNumbers) {
        PriorityNumbers = priorityNumbers;
    }

    public String getEarliestPriorityDate() {
        return EarliestPriorityDate;
    }

    public void setEarliestPriorityDate(String earliestPriorityDate) {
        EarliestPriorityDate = earliestPriorityDate;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getApplicants() {
        return Applicants;
    }

    public void setApplicants(String applicants) {
        Applicants = applicants;
    }

    public String getInventors() {
        return Inventors;
    }

    public void setInventors(String inventors) {
        Inventors = inventors;
    }

    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getAbstract() {
        return Abstract;
    }

    public void setAbstract(String anAbstract) {
        Abstract = anAbstract;
    }

    public String getMainClaim() {
        return MainClaim;
    }

    public void setMainClaim(String mainClaim) {
        MainClaim = mainClaim;
    }

    public String getHasFullText() {
        return HasFullText;
    }

    public void setHasFullText(String hasFullText) {
        HasFullText = hasFullText;
    }

    public String getCitedbyPatentCount() {
        return CitedbyPatentCount;
    }

    public void setCitedbyPatentCount(String citedbyPatentCount) {
        CitedbyPatentCount = citedbyPatentCount;
    }

    public String getSimpleFamilySize() {
        return SimpleFamilySize;
    }

    public void setSimpleFamilySize(String simpleFamilySize) {
        SimpleFamilySize = simpleFamilySize;
    }

    public String getExtendedFamilySize() {
        return ExtendedFamilySize;
    }

    public void setExtendedFamilySize(String extendedFamilySize) {
        ExtendedFamilySize = extendedFamilySize;
    }

    public String getSequenceCount() {
        return SequenceCount;
    }

    public void setSequenceCount(String sequenceCount) {
        SequenceCount = sequenceCount;
    }

    public String getCPCClassifications() {
        return CPCClassifications;
    }

    public void setCPCClassifications(String CPCClassifications) {
        this.CPCClassifications = CPCClassifications;
    }

    public String getIPCRClassifications() {
        return IPCRClassifications;
    }

    public void setIPCRClassifications(String IPCRClassifications) {
        this.IPCRClassifications = IPCRClassifications;
    }

    public String getUSClassifications() {
        return USClassifications;
    }

    public void setUSClassifications(String USClassifications) {
        this.USClassifications = USClassifications;
    }

    public String getNPLCitationCount() {
        return NPLCitationCount;
    }

    public void setNPLCitationCount(String NPLCitationCount) {
        this.NPLCitationCount = NPLCitationCount;
    }

    public String getNPLResolvedCitationCount() {
        return NPLResolvedCitationCount;
    }

    public void setNPLResolvedCitationCount(String NPLResolvedCitationCount) {
        this.NPLResolvedCitationCount = NPLResolvedCitationCount;
    }

    public String getNPLResolvedLensIDs() {
        return NPLResolvedLensIDs;
    }

    public void setNPLResolvedLensIDs(String NPLResolvedLensIDs) {
        this.NPLResolvedLensIDs = NPLResolvedLensIDs;
    }

    public String getNPLResolvedExternalIds() {
        return NPLResolvedExternalIds;
    }

    public void setNPLResolvedExternalIds(String NPLResolvedExternalIds) {
        this.NPLResolvedExternalIds = NPLResolvedExternalIds;
    }

    public String getNPLCitations() {
        return NPLCitations;
    }

    public void setNPLCitations(String NPLCitations) {
        this.NPLCitations = NPLCitations;
    }

}