package com.example.patentcoders.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.patentcoders.Patent;
import com.example.patentcoders.R;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import static java.lang.System.in;

public class PatentClusteringActivity extends Fragment {
    private final static int CHOOSE_FILE = 100;
    private final static String TAG = "PAnalysisActivity";
   List<Patent> patentsList;

    Button fileUploadBtn;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.patentanalysis_activity, container,false);
        fileUploadBtn = (Button) view.findViewById(R.id.fileUploadBtn);
        fileUploadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFile();
            }
        });
        patentsList = new ArrayList<>();
        return view;
    }

    void openFile() {
        try {
            parseCSVFile(null);
        }catch (Exception ex) {
            Log.e(TAG, ex.getMessage());
        }
//        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
//        intent.addCategory(Intent.CATEGORY_OPENABLE);
//        intent.setType("text/csv");
//        startActivityForResult(Intent.createChooser(intent, "Open CSV"), CHOOSE_FILE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == CHOOSE_FILE) {
            try {
                parseCSVFile(new File(data.getData().getPath()));
            }catch(Exception ex) {
                Log.e(TAG, ex.getMessage());
            }
        }
    }

    private void parseCSVFile(File csvFile) throws IOException, FileNotFoundException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(getActivity().getAssets().open("lens_export.csv")));
        String line = "";
        while((line = reader.readLine()) != null) {
            Log.d(TAG, line);
            Patent patent = new Patent(line);
            patentsList.add(patent);
        }
        Log.d(TAG, "patents parsed");
        reader.close();
    }

}
