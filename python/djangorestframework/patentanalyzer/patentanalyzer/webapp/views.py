from django.shortcuts import render
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from django.http import JsonResponse
from django.core import serializers
from django.conf import settings
import json
from . import util

# Create your views here.
@api_view(["POST"])
def GetPatentClusters(self):
	try:
			json_data = util.preprocess_data(self);
			return Response(json_data)
	except ValueError as e:
		return Response(e.args[0], status.HTTP_400_BAD_REQUEST)

@api_view(["POST"])
def GetStats(request):
	try:
		json_stats = util.display_stats(request.data)
		return Response(json_stats)
	except ValueError as e:
		return Response(e.args[0],status.HTTP_400_BAD_REQUEST)		
