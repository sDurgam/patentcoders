import pandas as pd
import numpy as np
import os
import json
import csv

def explode_str(df, col, sep):
    s = df[col]
    i = np.arange(len(s)).repeat(s.str.count(sep) + 1)
    return df.iloc[i].assign(**{col: sep.join(s).split(sep)})

def preprocess_data(self):
    with open(os.path.join( os.getcwd(), './static/input.csv'), encoding="utf8", errors='ignore') as f:
        df = pd.read_csv(f)
        input_df = df[['Jurisdiction','Kind','Publication Number','Publication Date','Publication Year','Title','Applicants','Inventors','Type','Abstract','Main Claim','Simple Family Size','IPCR Classifications']]
    expand_df = explode_str(input_df,'IPCR Classifications', ';;')
    
    pub_no = "Publication Number"
    df = expand_df[pub_no].str.rsplit(' ',1).to_frame()
    expand_df[['Publication Number','temp']] = pd.DataFrame(df[pub_no].values.tolist(), index= df.index)
    expand_df = expand_df.drop(['temp'], axis=1)
    var = 'IPCR Classifications'
    new = expand_df[var].str.split("/", n = 1, expand = True)
    expand_df['IPCR Classifications']= new[0] 
    with open(r'preprocessed_dataset.csv', 'w') as f:
        expand_df.to_csv(f)
    df_apr = expand_df.drop(['Jurisdiction','Kind','Publication Date','Publication Year','Title','Applicants','Inventors','Type','Abstract','Main Claim','Simple Family Size'], axis =1)
    unq_codes = df_apr[var].unique()
    d_step1={}
    for code in unq_codes:
        v = str(code)
        tmp = [(df_apr[df_apr[var] ==code] 
            .groupby(['Publication Number'])
            .sum().unstack().reset_index().fillna(0) 
            .set_index('Publication Number'))]
        d_step1[v] = list(tmp[0]['level_0'].index)
    with open(r'step1_clusters.csv', 'w') as f:
        for key in d_step1.keys():
            f.write("%s,%s\n"%(key,d_step1[key]))
    with open(r'result_step1.json', 'w+') as fp:
        json.dump(d_step1, fp)
    expand_df = explode_str(input_df,'Applicants', ';;')
    d={}
    a = expand_df['Jurisdiction'].value_counts()
    d['Jurisdiction'] = list(zip(a,a.index))
    a = expand_df['Kind'].value_counts()
    d['Kind'] = list(zip(a,a.index))
    a = expand_df['Inventors'].value_counts()
    d['Inventors'] = list(zip(a,a.index))
    a = expand_df['Applicants'].value_counts()
    d['Applicants'] = list(zip(a,a.index))
    a = expand_df['Type'].value_counts()
    d['Type'] = list(zip(a,a.index))
    a = expand_df['Simple Family Size'].value_counts()
    d['Simple Family Size'] = list(zip(a,a.index))
    with open(r'result_step2.json', 'w+') as fp:
        json.dump(d, fp)
    return d_step1

def display_stats(args):
    json_output = {};
    my_path = os.path.abspath(os.path.dirname(__file__))
    path = os.path.join(my_path, "../result_step2.json")
    with open(path) as json_file:
        data = json.load(json_file)
    for arg in args:
        data[arg].insert(0, "[total: " + str(len(data[arg])) + ']')
        json_output[str(arg)] = data[arg]
        
    return json_output

                